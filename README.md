# Visual Crossing weather fetcher

The code in this repository is an automated way to fetch weather data from the [Visual Crossing API](https://www.visualcrossing.com/weather-api). 
It is meant to be used as a data source for other applications, such as Looker.
The main application is built in Django, and is using the Visual Crossing API to obtain its data. 
There is still code to import from other data sources, but they are not maintained anymore,
and will be removed in a future version.

<img src="data/screenshot_location_admin.png" width="600" alt="Screenshot of location Amsterdam with weather measurmements in Django admin panel">

## Disclaimer

This code is not meant to be used in production. It is not tested, and it is not optimized. It is meant to be a
starting point for a project that you can build on top of. It is not meant to be used as is.

The author is not affiliated with Visual Crossing in any way. This repository is not endorsed by Visual Crossing.

## Usage

### Installation

I advize you to use a virtual environment for this project. This is how I created my virtual environment:

```bash
pyenv install 3.11.0
pyenv virtualenv 3.11.0 visual-crossing
echo "visual-crossing" > .python-version
```

Note that the file `.python-version` is in .gitignore. If you ever want to remove the virtual environment again,
you can use the following command:

```bash
pyenv uninstall visual-crossing
```

Next step, install the requirements:

```bash
pip install -r requirements.txt
```

After installing the requirements, you can populate a local database and run the code with:

```bash
python manage.py migrate
python manage.py createsuperuser
python manage.py create_locations
python manage.py runserver
```

The application will be available at http://localhost:8000/admin.

### Locations

You can then add locations from the Location admin panel. Make sure to give it a proper name, since that is what 
will be passed to Visual Crossing API. There is a validation though that the specified location and the location 
according to Visual Crossing API is in close proximity to avoid unintended fetches. There is the Django command 
`create_locations` that will create a handful of locations for you. You can use it as a starting point. Read its 
help for more information.

### Visual Crossing API

You need an account on Visual Crossing in order to make use of the API. There is a free account that lets you fetch 
up to 1,000 measurements per day. You should create a `secrets.json` file in the directory `visual-crossing/settings` 
with the following content:

```json
{
  "SECRET_KEY": "Django secret key",
  "VISUAL_CROSSING_API_KEY": "Your API key"
}
```

The file `secrets.json` in that directory is already in `.gitignore`. Make sure you do not publish your secrets to
your repository! You can generate a Django secret key with the following command:

```bash
python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'
```

Note that the file `encrypted_secrets.json` is for the author of this repository only. It contains his encrypted
version of the `secrets.json` file.

### Measurements

Once you have the locations and API key set up, you can use the `fetch_measurements` action from the Location admin
panel to fetch measurements from the API. A single measurement is always per Location per day. It fetches data for 
the selected locations, most recent data that is not in the database yet first, until it reaches the API limit. It 
will then stop and you can run it again the next day.

❗ WARNING ❗The code in this repository is written with a free account in mind, and only tested as such. For instance,
when fetching data, it will continue to fetch data until it receives a status code 429 from the API. If you have a
metered account, you should change the code to stop fetching data in order to not be charged a large amount of money.

### Data analysis

For now, there are only limited analytics capabilities in the application. 

- You can use the `export_raw` Django command to export the data for a given location to a csv file.
- You can use the export action from the Locations admin panel for the same purpose.
- You can play around with the `plot` Django command, generating charts in the `charts` folder inside `data`.

<img src="data/max_temp_per_year_amsterdam.png" width="1500" alt="Screenshot of location Amsterdam with weather measurmements in Django admin panel">


## Roadmap

I want to add the following capabilities to this repository:

- Export to BigQuery
- Optimization of queries and API calls

Things that I am not planning on building:

- Analyze the weather
  - I intend to use Looker for that, for which I need the data in BigQuery

## Requirements

There are the following requirements to run the product:

- `Django`: To build the application
- `haversine`: To calculate the distance between two locations
- `requests`: To make API calls to Visual Crossing API
- `unidecode`: To convert unicode characters to ASCII characters, used in `Location.code()`

To develop the application, there are the following requirements:

- `django-debug-toolbar`: To debug the application

## Architecture

This Django project has a strict one-way app dependency tree. An app can only import from apps that are 
strictly higher in the tree. The tree is as follows:

1. `visual-crossing`, `common`
2. `locations`
3. `measurements`
4. `data_sources`, `visuals`

As an example: both measurements and data_sources can import from locations, but not the other way around.
Therefore, the admin panels for measurements and data_sources inject functionality into the admin panels
of locations.

## Contact

If you have any questions, don't hesitate to contact me at the email address on my Codeberg profile.
