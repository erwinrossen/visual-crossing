from django.contrib import admin

from locations.models import Location


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    fields = ('name', 'code', 'latitude', 'longitude', 'altitude')
    list_display = ('name', 'code', 'latitude', 'longitude', 'altitude')
    readonly_fields = ('code',)
    ordering = ('name',)
    search_fields = ('name',)
