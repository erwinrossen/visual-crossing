from django.db import models
from haversine import haversine
from unidecode import unidecode


class Location(models.Model):
    help_latitude = 'Latitude in decimal degrees, positive for north, negative for south'
    help_longitude = 'Longitude in decimal degrees, positive for east, negative for west'
    help_altitude = 'Altitude in meters above sea level'

    name = models.CharField(max_length=128, unique=True)
    latitude = models.DecimalField(decimal_places=3, max_digits=6)
    longitude = models.DecimalField(decimal_places=3, max_digits=6)
    altitude = models.DecimalField(decimal_places=1, max_digits=6, default=0)

    @property
    def code(self) -> str:
        """
        Return the name as a code

        A code only contains ascii lowercase letters, digits, and underscores.
        """

        code = unidecode(self.name)
        code = ''.join(c if (c.isalnum() or c == '_') else '_' for c in code)
        while '__' in code:
            code = code.replace('__', '_')
        code = code.strip('_').lower()
        return code

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f'Location(name={self.name}, longitude={self.longitude}, ' \
               f'latitude={self.latitude}, altitude={self.altitude})'

    def __eq__(self, other) -> bool:
        if isinstance(other, Location):
            return self.name == other.name
        return False

    def __sub__(self, other) -> float:
        if isinstance(other, Location):
            return haversine((self.latitude, self.longitude), (other.latitude, other.longitude))
        return NotImplemented
