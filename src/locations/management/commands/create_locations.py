import logging
from django.core.management import BaseCommand

from locations.models import Location

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Create or update some default list of locations in the database'

    def handle(self, *args, **options):
        locations = [
            Location(name='Amsterdam', latitude=52.3699, longitude=4.90788, altitude=-2),
            Location(name='Berlin', latitude=52.516, longitude=13.3769, altitude=35),
            Location(name='Hamburg', latitude=53.5510846, longitude=9.9936, altitude=6),
            Location(name='La Paz', latitude=-16.5, longitude=-68.15, altitude=3650),
            Location(name='Düsseldorf', latitude=51.233333, longitude=6.783333, altitude=38),
            Location(name='Москва', latitude=55.755833, longitude=37.617222, altitude=152),
            Location(name='北京', latitude=39.906667, longitude=116.3975, altitude=50),
        ]
        Location.objects.bulk_create(locations, ignore_conflicts=True)
