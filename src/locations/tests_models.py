from django.test import TestCase

from locations.models import Location


class LocationTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.amsterdam = Location.objects.create(name='Amsterdam', latitude=52.3699, longitude=4.90788, altitude=-2)
        cls.hamburg = Location.objects.create(name='Hamburg', latitude=53.5510846, longitude=9.9936, altitude=6)
        cls.berlin = Location.objects.create(name='Berlin', latitude=52.516, longitude=13.3769, altitude=35)
        cls.dusseldorf = Location.objects.create(name='Düsseldorf', latitude=51.233333, longitude=6.783333, altitude=38)
        cls.la_paz = Location.objects.create(name='La Paz', latitude=-16.500, longitude=-68.150, altitude=3650)
        cls.moscow = Location.objects.create(name='Москва', latitude=55.755833, longitude=37.617222, altitude=152)
        cls.beijing = Location.objects.create(name='北京', latitude=39.906667, longitude=116.3975, altitude=50)

    def test_distance(self):
        self.assertAlmostEqual(self.amsterdam - self.hamburg, 365, places=0)
        self.assertAlmostEqual(self.amsterdam - self.berlin, 574, places=0)
        self.assertAlmostEqual(self.amsterdam - self.dusseldorf, 181, places=0)
        self.assertAlmostEqual(self.amsterdam - self.la_paz, 10_354, places=0)
        self.assertAlmostEqual(self.amsterdam - self.moscow, 2_147, places=0)
        self.assertAlmostEqual(self.amsterdam - self.beijing, 7_821, places=0)
        self.assertAlmostEqual(self.hamburg - self.berlin, 254, places=0)

    def test_name(self):
        self.assertEqual('amsterdam', self.amsterdam.code)
        self.assertEqual('dusseldorf', self.dusseldorf.code)
        self.assertEqual('la_paz', self.la_paz.code)
        self.assertEqual('moskva', self.moscow.code)
        self.assertEqual('bei_jing', self.beijing.code)
