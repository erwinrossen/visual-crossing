from itertools import groupby
from operator import attrgetter, itemgetter
from typing import Callable, Collection, Optional, Tuple, TypeVar, Union

T = TypeVar('T')
U = TypeVar('U')


def group_by_attr(items: Collection, *attrs: str):
    """
    Group the items by the attributes returned by attrs. The input `items` does not need to be sorted

    Example of grouping by 1 attribute
    >>> class Person:
    ...     def __init__(self, name, age):
    ...         self.name = name
    ...         self.age = age
    ...     def __repr__(self):
    ...         return f'Person({self.name}, {self.age})'
    >>> people = [Person('Alice', 20), Person('Bob', 20), Person('Charlie', 30)]
    >>> for age, people in group_by_attr(people, 'age'):
    ...     print(age, list(people))
    20 [Person(Alice, 20), Person(Bob, 20)]
    30 [Person(Charlie, 30)]

    Example of grouping by 2 attributes
    >>> class Person:
    ...     def __init__(self, name, gender, age):
    ...         self.name = name
    ...         self.gender=gender
    ...         self.age = age
    ...     def __repr__(self):
    ...         return f'Person({self.name}, {self.age})'
    >>> people = [Person('Alice', 'F', 20), Person('Betty', 'F', 20),
    ...           Person('Charlotte', 'F', 30), Person('Delilah', 'F', 30),
    ...           Person('Adam', 'M', 20), Person('Bob', 'M', 20),
    ...           Person('Chris', 'M', 30), Person('David', 'M', 30)]
    >>> for age, people in group_by_attr(people, 'age', 'gender'):
    ...     print(age, list(people))
    (20, 'F') [Person(Alice, 20), Person(Betty, 20)]
    (20, 'M') [Person(Adam, 20), Person(Bob, 20)]
    (30, 'F') [Person(Charlotte, 30), Person(Delilah, 30)]
    (30, 'M') [Person(Chris, 30), Person(David, 30)]
    """

    return _groupby_properly(items, keyfun=attrgetter(*attrs))


def group_by_item(items: Collection, *itemkeys: Union[int, str]):
    """
    Group the items by the items returned by itemkeys. The input `items` does not need to be sorted

    Example of grouping by 1 item
    >>> people = [('Alice', 20), ('Bob', 20), ('Charlie', 30)]
    >>> for age, people in group_by_item(people, 1):
    ...     print(age, list(people))
    20 [('Alice', 20), ('Bob', 20)]
    30 [('Charlie', 30)]

    Example of grouping by dictionary key:
    >>> people = [{'name': 'Alice', 'age': 20}, {'name': 'Bob', 'age': 20}, {'name': 'Charlie', 'age': 30}]
    >>> for age, people in group_by_item(people, 'age'):
    ...     print(age, list(people))
    20 [{'name': 'Alice', 'age': 20}, {'name': 'Bob', 'age': 20}]
    30 [{'name': 'Charlie', 'age': 30}]

    Example of grouping by 2 items
    >>> people = [('Alice', 'F', 20), ('Betty', 'F', 20),
    ...           ('Charlotte', 'F', 30), ('Delilah', 'F', 30),
    ...           ('Adam', 'M', 20), ('Bob', 'M', 20),
    ...           ('Chris', 'M', 30), ('David', 'M', 30)]
    >>> for age, people in group_by_item(people, 2, 1):
    ...     print(age, list(people))
    (20, 'F') [('Alice', 'F', 20), ('Betty', 'F', 20)]
    (20, 'M') [('Adam', 'M', 20), ('Bob', 'M', 20)]
    (30, 'F') [('Charlotte', 'F', 30), ('Delilah', 'F', 30)]
    (30, 'M') [('Chris', 'M', 30), ('David', 'M', 30)]
    """

    return _groupby_properly(items, keyfun=itemgetter(*itemkeys))


def _sort_descriptor_for_optional(fun: Callable[[T], Optional[U]]) -> Callable[[T], Tuple[bool, Optional[U]]]:
    """
    Given a function T -> Optional[U], this returns a function T -> (bool, Optional[U]), where "bool" is a flag that
    marks the value of the key U as being None or not. This is a useful transformation because the keys obtained from
    a function of the form T -> (bool, Optional[U]) are guaranteed to be totally orderable whereas the keys
    obtained from T -> Optional[U] are not.

    This works because when comparing tuples, (A, B), they are first ordered by As and then the Bs are compared
    within their A groups. Meaning that because all U values are in a tuple (False, U) and all Nones are in a tuple
    (True, None), we'll never compare Us with Nones.

    For example, given the function
    >>> get_id = lambda item: item.get('id')
    >>> dicts = [{'id': 0, 'color': 'green'}, {'id': 1, 'color': 'blue'}, {'color': 'red'}]

    # The following list cannot be sorted
    >>> [get_id(x) for x in dicts]
    [0, 1, None]

    # However the following list can be sorted
    >>> [_sort_descriptor_for_optional(get_id)(x) for x in dicts]
    [(False, 0), (False, 1), (True, None)]
    """

    def total_order_key(item: T) -> Tuple[bool, U]:
        key = fun(item)
        return key is None, key

    return total_order_key


def _groupby_properly(items: Collection[T], keyfun: Callable[[T], U]):
    """
    Group the items by the key returned by keyfun. The input `items` does not need to be sorted

    With an unsorted list, Python's groupby does not work properly:
    >>> for key, values in groupby([1, 2, 3, 4, 5, 6], lambda x: x % 2):
    ...     print(key, list(values))
    1 [1]
    0 [2]
    1 [3]
    0 [4]
    1 [5]
    0 [6]
    >>> for key, values in _groupby_properly([1, 2, 3, 4, 5, 6], lambda x: x % 2):
    ...     print(key, list(values))
    0 [2, 4, 6]
    1 [1, 3, 5]
    """

    # We transform keyfun for ordering, but use the given keyfun for grouping
    return groupby(sorted(items, key=_sort_descriptor_for_optional(keyfun)), key=keyfun)
