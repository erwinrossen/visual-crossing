import string

from decimal import Decimal
from typing import Union


def parse_float(number: Union[str, int, float]) -> float:
    """
    Convert the input string that represents a number to a Decimal with N decimal places

    >>> parse_float('€4.-')
    4.0
    """

    try:
        if isinstance(number, str):
            thousands_separator = ','
            decimal_separator = '.'
            if ',' in number:
                if '.' not in number or ('.' in number and number.index('.') < number.index(',')):
                    # When there are both thousands and decimal separators in use, we assume that
                    # the first one is the thousands separator and the second one the decimal separator
                    thousands_separator = '.'
                    decimal_separator = ','

            number = number.replace(thousands_separator, '')

            # If there are multiple decimal separators present, we assume they are actually a thousands separator
            if number.count(decimal_separator) > 1:
                number = number.replace(decimal_separator, '')

            # Ensure that the number passed to float() uses a period as decimal separator
            number = number.replace(decimal_separator, '.')

        try:
            number = float(number)
        except ValueError:
            # It could be that there are other characters around the decimal, e.g. currency symbols
            # First, strip all non-digit or separator characters from the string. We try again after
            # stripping all non-digit related characters. We cannot do this immediately, since the
            # original text might have been valid with these characters, e.g. 8.7E12 or -13.
            number = float(''.join(char for char in number if char in string.digits + '.'))

        return number

    except Exception as e:
        exception_class = type(e)
        msg = f"Cannot convert '{number}' to float"
        raise exception_class(msg) from e


def parse_decimal(number: Union[str, int, float, Decimal], nr_decimals: int = 2) -> Decimal:
    """
    Convert the input string that represents a number to a Decimal with N decimal places

    >>> parse_decimal('€4.-')
    Decimal('4.00')
    >>> parse_decimal(4)
    Decimal('4.00')
    >>> parse_decimal(4.1234)
    Decimal('4.12')
    >>> parse_decimal(Decimal('4.1234'))
    Decimal('4.12')
    """

    try:
        number = parse_float(number)
        return Decimal(f'{number:.{nr_decimals}f}')
    except Exception as e:
        exception_class = type(e)
        msg = f"Cannot convert '{number}' to Decimal"
        raise exception_class(msg) from e


def parse_int(number: Union[str, int, float]) -> int:
    """
    Convert the input string that represents a number to an integer, rounding when necessary

    >>> parse_int('€4.-')
    4
    """

    try:
        number_as_decimal = parse_decimal(number, nr_decimals=0)
        return int(number_as_decimal)
    except Exception as e:
        exception_class = type(e)
        msg = f"Cannot convert '{number}' to integer"
        raise exception_class(msg) from e
