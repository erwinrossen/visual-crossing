from unittest import skipIf

import os


def full_suite():
    """
    Decorator for test cases to skip the decorated test case unless the user runs the full test suite.

    A user running the tests can specify which test suite they want to run (FAST or FULL) by providing this as an
    environment variable called SUITE. If no suite is explicitly set, the FAST suite will be run.

    To run the full suite, run the following terminal command:

    SUITE=FULL python manage.py test
    """

    suite = os.environ.get('SUITE')
    full_suite_requested = suite == 'FULL'
    msg = 'Test case skipped, since it is part of the FULL test suite and the FAST test suite is running'
    return skipIf(not full_suite_requested, msg)
