from decimal import Decimal
from django.test import TestCase
from django.utils import timezone

from locations.models import Location
from measurements.models import Measurement
from measurements.utils import calc_avg, calc_sum

T1 = timezone.datetime(2023, 1, 1, tzinfo=timezone.utc)
T2 = timezone.datetime(2023, 1, 2, tzinfo=timezone.utc)
T3 = timezone.datetime(2023, 1, 3, tzinfo=timezone.utc)
T4 = timezone.datetime(2023, 1, 4, tzinfo=timezone.utc)
T5 = timezone.datetime(2023, 1, 5, tzinfo=timezone.utc)


class UtilsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.amsterdam = Location.objects.create(name='Amsterdam', latitude=52.3699, longitude=4.90788, altitude=-2)
        Measurement.objects.bulk_create([
            Measurement(location=cls.amsterdam, day=T1, temperature_avg=5, precipitation=2),
            Measurement(location=cls.amsterdam, day=T2, temperature_avg=7, precipitation=None),
            Measurement(location=cls.amsterdam, day=T3, temperature_avg=None, precipitation=5),
            Measurement(location=cls.amsterdam, day=T4, temperature_avg=None, precipitation=None),
            Measurement(location=cls.amsterdam, day=T5, temperature_avg=-5, precipitation=0),
        ])

    def test_calc_avg(self):
        self.assertEqual(Decimal('2.3'), calc_avg(self.amsterdam.measurements.all(), 'temperature_avg'))
        self.assertEqual(Decimal('2.3'), calc_avg(self.amsterdam.measurements.all(), 'precipitation'))

    def test_sum_avg(self):
        self.assertEqual(Decimal('7.0'), calc_sum(self.amsterdam.measurements.all(), 'precipitation'))
