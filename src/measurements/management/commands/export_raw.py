import datetime
import logging
from django.core.management import BaseCommand

from locations.models import Location
from measurements.csv_exporter import CsvExporter

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Export raw data for the given location in csv format'

    def handle(self, *args, **options):
        locations = options.get('locations')
        start = options.get('start')
        end = options.get('end')

        if locations is not None:
            all_locations = Location.objects.all()
            print([location.code for location in all_locations])
            locations = [location for location in all_locations if location.code in locations]
        if start is not None:
            start = datetime.datetime.strptime(start, '%Y-%m-%d').date()
        if end is not None:
            end = datetime.datetime.strptime(end, '%Y-%m-%d').date()

        exporter = CsvExporter(locations=locations, start_day=start, end_day=end)
        exporter.export()

        print(f'Successfully written {len(exporter.measurements)} measurements '
              f'to {exporter.path_to_file}')

    def add_arguments(self, parser):
        help_text_locations = 'A space-separated list of location(s) to export measurements for. ' \
                              'If not given, all locations are exported.'
        help_text_start = 'The start date to export measurements from in the format YYYY-MM-DD.'
        help_text_end = 'The end date to export measurements to in the format YYYY-MM-DD.'
        parser.add_argument('--locations', type=str, nargs='+', help=help_text_locations)
        parser.add_argument('--start', type=str, help=help_text_start)
        parser.add_argument('--end', type=str, help=help_text_end)
