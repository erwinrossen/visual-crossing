from django.contrib import admin, messages
from django.http import HttpResponse
from django_admin_inline_paginator.admin import TabularInlinePaginated

from locations.admin import LocationAdmin
from measurements.csv_exporter import CsvExporter
from measurements.models import Measurement


@admin.register(Measurement)
class MeasurementAdmin(admin.ModelAdmin):
    exclude = None

    list_display = ('location', 'day',
                    'temperature_avg', 'temperature_min', 'temperature_max',
                    'feels_like_temperature_avg', 'feels_like_temperature_min', 'feels_like_temperature_max',
                    'wind_speed', 'precipitation', 'precipitation_duration')
    list_filter = (('location', admin.RelatedOnlyFieldListFilter),)
    ordering = ('location', 'day')


class MeasurementInline(TabularInlinePaginated):
    model = Measurement
    fields = ('day',
              'temperature_avg', 'temperature_min', 'temperature_max',
              'feels_like_temperature_avg', 'feels_like_temperature_min', 'feels_like_temperature_max',
              'wind_speed', 'precipitation', 'precipitation_duration')
    readonly_fields = fields
    extra = 0
    ordering = ('-day',)
    per_page = 100

    show_change_link = False

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('location')

    def has_add_permission(self, request, obj=None):
        return False


def export_measurements_as_csv(_modeladmin, request, queryset):
    """
    Export the given range of measurements to a csv file, usable by Google Data Studio
    """

    try:
        exporter = CsvExporter(locations=queryset)
        exporter.export()

        response = HttpResponse(content_type='application/txt')
        response['Content-Disposition'] = f'attachment;filename="{exporter.filename}"'
        with open(exporter.path_to_file, 'rb') as f:
            response.write(f.read())

        # The contents of the files are in memory, so we can delete the file
        exporter.path_to_file.unlink()

        # TODO: this message should be displayed after the response is processed (file is downloaded)
        msg = f'Exported {len(exporter.measurements)} measurements for {queryset.count()} locations'
        messages.success(request, msg)
        return response
    except Exception as e:
        messages.error(request, f'Failed to export weather: {e}')


# Add these properties dynamically on Location admin, to prevent a dependency from locations to measurements
additional_fields = ('nr_measurements', 'first_measurement_from', 'last_measurement_from')
LocationAdmin.list_display = (*LocationAdmin.list_display, *additional_fields)

# Add these inlines dynamically on Location admin, to prevent a dependency from locations to measurements
LocationAdmin.inlines = (*LocationAdmin.inlines, MeasurementInline)

# Add these actions dynamically on Location admin, to prevent a dependency from locations to measurements
LocationAdmin.actions = (*LocationAdmin.actions, export_measurements_as_csv)
