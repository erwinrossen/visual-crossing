from datetime import date
from decimal import Decimal
from django.db import models
from typing import Optional, Any, Dict, TypedDict

from locations.models import Location


class MeasurementDict(TypedDict):
    location: str
    latitude: Decimal
    longitude: Decimal
    altitude: Decimal
    day: str  # In format YYYY-MM-DD
    temperature_avg: Optional[Decimal]
    temperature_min: Optional[Decimal]
    temperature_max: Optional[Decimal]
    feels_like_temperature_avg: Optional[Decimal]
    feels_like_temperature_min: Optional[Decimal]
    feels_like_temperature_max: Optional[Decimal]
    wind_speed: Optional[Decimal]
    precipitation: Optional[Decimal]
    precipitation_duration: Optional[Decimal]


class Measurement(models.Model):
    decimal_settings = {'decimal_places': 1, 'max_digits': 8, 'null': True, 'blank': True, 'default': None}

    location = models.ForeignKey(Location, related_name='measurements', on_delete=models.CASCADE)
    day = models.DateField()  # DB indexing is done using the unique_together with location

    temperature_avg = models.DecimalField(
        verbose_name='Average temperature (°C)',
        help_text='Gemiddelde temperatuur (in graden Celsius) van de dag op 1.50 m hoogte',
        **decimal_settings)
    temperature_min = models.DecimalField(
        verbose_name='Minimum temperature (°C)',
        help_text='Minimum temperatuur (in graden Celsius) van de dag op 1.50 m hoogte',
        **decimal_settings)
    temperature_max = models.DecimalField(
        verbose_name='Maximum temperature (°C)',
        help_text='Maximum temperatuur (in graden Celsius) van de dag op 1.50 m hoogte',
        **decimal_settings)

    feels_like_temperature_avg = models.DecimalField(
        verbose_name='Average feels-like temperature (°C)',
        help_text='Gemiddelde gevoelstemperatuur (in graden Celsius) van de dag op 1.50 m hoogte',
        **decimal_settings)
    feels_like_temperature_min = models.DecimalField(
        verbose_name='Minimum feels-like temperature (°C)',
        help_text='Minimum gevoelstemperatuur (in graden Celsius) van de dag op 1.50 m hoogte',
        **decimal_settings)
    feels_like_temperature_max = models.DecimalField(
        verbose_name='Maximum feels-like temperature (°C)',
        help_text='Maximum gevoelstemperatuur (in graden Celsius) van de dag op 1.50 m hoogte',
        **decimal_settings)

    wind_speed = models.DecimalField(
        verbose_name='Average wind speed (km/h)',
        help_text='Etmaalgemiddelde windsnelheid (in km/h) op 10m hoogte',
        **decimal_settings)
    precipitation = models.DecimalField(
        verbose_name='Precipitation (mm)',
        help_text='Dagsom van de neerslag (in mm)',
        **decimal_settings)
    precipitation_duration = models.DecimalField(
        verbose_name='Precipitation duration (hours)',
        help_text='Duur van de neerslag (in uren) per dag',
        **decimal_settings)

    class Meta:
        unique_together = ('day', 'location')

    def as_dict(self) -> Dict[str, Any]:
        return {
            'location': self.location.name,
            'latitude': self.location.latitude,
            'longitude': self.location.longitude,
            'altitude': self.location.altitude,
            'day': f'{self.day:%Y-%m-%d}',
            'temperature_avg': self.temperature_avg,
            'temperature_min': self.temperature_min,
            'temperature_max': self.temperature_max,
            'feels_like_temperature_avg': self.feels_like_temperature_avg,
            'feels_like_temperature_min': self.feels_like_temperature_min,
            'feels_like_temperature_max': self.feels_like_temperature_max,
            'wind_speed': self.wind_speed,
            'precipitation': self.precipitation,
            'precipitation_duration': self.precipitation_duration,
        }

    def __str__(self):
        return f'{self.location.name} ({self.day:%Y-%m-%d})'

# Add these properties dynamically on Location, to prevent a dependency from locations to measurements

def nr_measurements(location: Location) -> int:
    """
    Return the number of measurements for a location.
    """

    return location.measurements.count()


def first_measurement_from(location: Location) -> Optional[date]:
    """
    Return the first measurement date for a location.
    """

    if last_measurement := location.measurements.order_by('day').first():
        return last_measurement.day
    else:
        return None


def last_measurement_from(location: Location) -> Optional[date]:
    """
    Return the last measurement date for a location.
    """

    if last_measurement := location.measurements.order_by('-day').first():
        return last_measurement.day
    else:
        return None


Location.nr_measurements = property(nr_measurements)
Location.first_measurement_from = property(first_measurement_from)
Location.last_measurement_from = property(last_measurement_from)
