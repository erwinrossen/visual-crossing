from pathlib import PosixPath

import csv

from django.conf import settings
from functools import cached_property

import datetime
from django.db.models import QuerySet
from typing import Optional, Collection

from locations.models import Location
from measurements.models import Measurement


class CsvExporter:
    EXPORT_FOLDER = settings.BASE_DIR / 'tmp'

    def __init__(self, locations: Optional[Collection[Location]] = None,
                 start_day: Optional[datetime.date] = None, end_day: Optional[datetime.date] = None):
        self.locations = list(locations) if locations is not None else None
        self.start_day = start_day
        self.end_day = end_day

    @cached_property
    def measurements(self) -> QuerySet:
        """
        Get the measurements to export
        """

        measurements = Measurement.objects.select_related('location')
        if self.locations is not None:
            measurements = measurements.filter(location__in=self.locations)
        if self.start_day is not None:
            measurements = measurements.filter(day__gte=self.start_day)
        if self.end_day is not None:
            measurements = measurements.filter(day__lte=self.end_day)
        measurements = measurements.order_by('-day', 'location__name')
        return measurements

    @cached_property
    def filename(self) -> str:
        """
        Get the filename to export the measurements to
        """

        filename = 'measurements'
        if self.locations is not None:
            location_codes = sorted([location.code for location in self.locations])
            filename+= '_' + '_'.join(location_codes)
        if self.start_day is not None:
            filename += f'_from_{self.start_day:%Y%m%d}'
        if self.end_day is not None:
            filename += f'_to_{self.end_day:%Y%m%d}'
        filename += '.csv'
        return filename

    @cached_property
    def path_to_file(self) -> PosixPath:
        """
        Get the path to the file to export the measurements to
        """

        return self.EXPORT_FOLDER / self.filename

    def export(self):
        """
        Export the given range of measurements to a csv file
        """

        if not self.measurements:
            raise ValueError('No measurements to export')

        measurements = [m.as_dict() for m in self.measurements]
        m = measurements[0]
        for k, v in m.items():
            print(k, type(v))
        with open(self.path_to_file, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=measurements[0].keys(), delimiter=';')
            writer.writeheader()
            writer.writerows(measurements)
