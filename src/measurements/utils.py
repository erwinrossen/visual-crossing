from decimal import Decimal
from typing import Optional, List

from measurements.models import Measurement


def calc_avg(measurements: List[Measurement], metric: str) -> Optional[Decimal]:
    """
    Calculate the average of the given metric for the given measurements

    :param measurements: List of measurements to calculate the average for
    :param metric: The metric to calculate the average for
    :return: The average of all non-null values for the given metric, or None if there are no non-null values
    """

    values: List[Optional[Decimal]] = [
        getattr(m, metric) for m in measurements
    ]
    available_values = [value for value in values if value is not None]
    if available_values:
        return (Decimal(sum(available_values)) / len(available_values)).quantize(Decimal('0.1'))
    else:
        return None


def calc_sum(measurements: List[Measurement], metric: str) -> Optional[Decimal]:
    """
    Calculate the sum of the given metric for the given measurements

    :param measurements: List of measurements to calculate the sum for
    :param metric: The metric to calculate the sum for
    :return: The sum of all non-null values for the given metric, or None if there are no non-null values
    """

    values: List[Optional[Decimal]] = [
        getattr(m, metric) for m in measurements
    ]
    available_values = [value for value in values if value is not None]
    if available_values:
        return Decimal(sum(available_values)).quantize(Decimal('1'))
    else:
        return None
