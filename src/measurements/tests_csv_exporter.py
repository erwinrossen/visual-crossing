import os.path
import shutil

import filecmp
from django.conf import settings
from django.test import TestCase
from django.utils import timezone
from pathlib import PosixPath

from locations.models import Location
from measurements.csv_exporter import CsvExporter
from measurements.models import Measurement

T1 = timezone.datetime(2023, 1, 1, tzinfo=timezone.utc)
T2 = timezone.datetime(2023, 1, 2, tzinfo=timezone.utc)
T3 = timezone.datetime(2023, 1, 3, tzinfo=timezone.utc)
T4 = timezone.datetime(2023, 1, 4, tzinfo=timezone.utc)
T5 = timezone.datetime(2023, 1, 5, tzinfo=timezone.utc)


class CsvExporterTestCase(TestCase):
    data_dir = settings.BASE_DIR / 'data' / 'test' / 'measurements'

    @classmethod
    def setUpTestData(cls):
        cls.amsterdam = Location.objects.create(name='Amsterdam', latitude=52.3699, longitude=4.90788, altitude=-2)
        cls.hamburg = Location.objects.create(name='Hamburg', latitude=53.5510846, longitude=9.9936, altitude=6)
        Measurement.objects.bulk_create(
            [
                Measurement(location=cls.amsterdam, day=T1, temperature_avg=5, precipitation=2),
                Measurement(location=cls.amsterdam, day=T2, temperature_avg=7, precipitation=None),
                Measurement(location=cls.amsterdam, day=T3, temperature_avg=None, precipitation=5),
                Measurement(location=cls.amsterdam, day=T4, temperature_avg=None, precipitation=None),
                Measurement(location=cls.amsterdam, day=T5, temperature_avg=-5, precipitation=0),
            ] + [
                Measurement(location=cls.hamburg, day=T1, temperature_avg=5, precipitation=12),
                Measurement(location=cls.hamburg, day=T2, temperature_avg=7, precipitation=None),
                Measurement(location=cls.hamburg, day=T3, temperature_avg=None, precipitation=15),
                Measurement(location=cls.hamburg, day=T4, temperature_avg=None, precipitation=None),
                Measurement(location=cls.hamburg, day=T5, temperature_avg=-5, precipitation=10),
            ]
        )

    def test_filename(self):
        exporter = CsvExporter(locations=Location.objects.filter(name='Amsterdam'), start_day=T1, end_day=T5)
        self.assertEqual('measurements_amsterdam_from_20230101_to_20230105.csv', exporter.filename)

        exporter = CsvExporter(start_day=T1, end_day=T5)
        self.assertEqual('measurements_from_20230101_to_20230105.csv', exporter.filename)

        exporter = CsvExporter(locations=Location.objects.filter(name='Amsterdam'), end_day=T5)
        self.assertEqual('measurements_amsterdam_to_20230105.csv', exporter.filename)

        exporter = CsvExporter(locations=Location.objects.filter(name='Amsterdam'), start_day=T1)
        self.assertEqual('measurements_amsterdam_from_20230101.csv', exporter.filename)

        exporter = CsvExporter()
        self.assertEqual('measurements.csv', exporter.filename)

    def test_path_to_file(self):
        exporter = CsvExporter()
        exporter.EXPORT_FOLDER = settings.BASE_DIR / 'path/to/export/folder'
        self.assertIn('path/to/export/folder/measurements.csv', str(exporter.path_to_file))

    def test_export(self):
        exporter = CsvExporter()
        exporter.EXPORT_FOLDER = settings.BASE_DIR / 'data' / 'test' / 'csv_output'
        if exporter.path_to_file.exists():
            # Delete the output file if it already exists
            exporter.path_to_file.unlink()
        self.assertFalse(exporter.path_to_file.exists())

        # Expected query: select relevant measurements and associated locations
        # SELECT * FROM "measurement" INNER JOIN "location" ON ("measurement"."location_id" = "location"."id")
        with self.assertNumQueries(1):
            exporter.export()

        self.assertTrue(exporter.path_to_file.exists())
        self.compare_file_with_file(exporter.path_to_file, self.data_dir / 'measurements.csv')

    def compare_file_with_file(self, input_filepath: PosixPath, reference_filepath: PosixPath) -> None:
        """
        Compare the input file with a reference file

        :param input_filepath: Full path to the input file to check
        :param reference_filepath: File name of the reference file. It is assumed this file exists in self.data_dir
        """

        if not filecmp.cmp(input_filepath, reference_filepath):
            reference_filename = os.path.basename(reference_filepath)
            generated_filename = f'generated_{reference_filename}'
            generated_filepath = self.data_dir / generated_filename
            shutil.copyfile(src=input_filepath, dst=generated_filepath)
            msg = f'Generated file written to: {generated_filename}'
            self.fail(msg)
