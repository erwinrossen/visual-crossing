from typing import Optional

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from django.conf import settings
from django.db.models import QuerySet
from matplotlib.axes import Axes


class Plotter:
    CHARTS_DIR = settings.BASE_DIR / 'data' / 'charts'

    def __init__(self, queryset: QuerySet):
        measurements = queryset.order_by('day').values('day', 'temperature_avg', 'temperature_min', 'temperature_max')
        df = pd.DataFrame.from_records(measurements)
        df['temperature_avg'] = df['temperature_avg'].astype(float)
        df['temperature_min'] = df['temperature_min'].astype(float)
        df['temperature_max'] = df['temperature_max'].astype(float)
        df['day'] = pd.to_datetime(df['day'])
        df['day_of_year'] = df['day'].dt.dayofyear
        self.df = df

    @property
    def color_map(self):
        """
        Returns a dictionary with a color for each year in the dataset, gradually changing from blue to red

        :return: {
            2014: '#0000FF',
            2015: '#1C00E2',
            2016: '#3800C6',
            2017: '#5500AA',
            2018: '#71008D',
            2019: '#8D0071',
            2020: '#AA0055',
            2021: '#C60038',
            2022: '#E2001C',
            2023: '#FF0000'
        }
        """

        # Compute the years in the dataset
        years = np.unique(self.df['day'].dt.year)

        # Define the starting and ending colors
        start_color = np.array([0, 0, 255])  # Blue
        end_color = np.array([255, 0, 0])  # Red

        # Create a dictionary of colors for each year
        color_dict = {}
        for year in years:
            # Compute the fraction of the way through the years list
            year_frac = (year - years.min()) / (years.max() - years.min())

            # Compute the linearly interpolated color for this year
            color = start_color + year_frac * (end_color - start_color)

            # Convert the color to the format that Matplotlib understands
            color_str = '#{:02X}{:02X}{:02X}'.format(*color.astype(int))

            # Add the color to the dictionary
            color_dict[year] = color_str

        return color_dict

    def plot_per_year(self, temp_identifier: str, smooth_days: int = 30):
        assert temp_identifier in ['avg', 'min', 'max'], 'temp_identifier must be one of "avg", "min", or "max"'

        self.df[f'temperature_{temp_identifier}_smooth'] = self.df[f'temperature_{temp_identifier}']. \
            rolling(smooth_days, center=True).mean()

        grouped = self.df.groupby(self.df['day'].dt.year)

        fig, ax = plt.subplots(figsize=(15, 8))
        for year, group in grouped:
            # We could specify a color from my own defined color map here, but that is unclear
            #  ax.plot(..., color=self.color_map[year])
            #
            # Another color map could be created like this, which is slightly more clear, but still not ideal
            #  color_map = cm.ScalarMappable(cmap='RdBu_r')
            #  color_map.set_clim(self.df['day'].min().year, self.df['day'].max().year)
            ax.plot(group['day_of_year'], group[f'temperature_{temp_identifier}_smooth'], label=year,
                    linewidth=1)

        self._style_chart(ax, title=f'{temp_identifier.title()} temperature in Amsterdam '
                                    f'({smooth_days} day rolling average)')
        path_to_output_file = self.CHARTS_DIR / f'{temp_identifier}_temp_per_year.png'
        plt.savefig(path_to_output_file)

    def plot_min_max_averaged_over_year(self, smooth_days: Optional[int] = None):
        # Calculate the mean average temperature, the minimum min temperature
        # and the maximum max temperature per day of the year over all years
        mean_df = self.df.groupby('day_of_year')['temperature_avg'].mean().reset_index()
        mean_df['temperature_min'] = self.df.groupby('day_of_year')['temperature_min'].min()
        mean_df['temperature_max'] = self.df.groupby('day_of_year')['temperature_max'].max()

        # Smooth these lines
        if smooth_days is not None:
            for identifier in ['avg', 'min', 'max']:
                mean_df[f'temperature_{identifier}_smooth'] = mean_df[f'temperature_{identifier}']. \
                    rolling(smooth_days, center=True).mean()

        # Plot these three lines
        fig, ax = plt.subplots(figsize=(15, 8))
        if smooth_days is not None:
            ax.plot(mean_df['day_of_year'], mean_df['temperature_avg_smooth'], color='black', label='Average')
            ax.plot(mean_df['day_of_year'], mean_df['temperature_min_smooth'], color='blue', label='Min')
            ax.plot(mean_df['day_of_year'], mean_df['temperature_max_smooth'], color='red', label='Max')
        else:
            ax.plot(mean_df['day_of_year'], mean_df['temperature_avg'], color='black', label='Average')
            ax.plot(mean_df['day_of_year'], mean_df['temperature_min'], color='blue', label='Min')
            ax.plot(mean_df['day_of_year'], mean_df['temperature_max'], color='red', label='Max')

        self._style_chart(ax, title='Average, min and max temperature in Amsterdam')
        path_to_output_file = self.CHARTS_DIR / 'avg_min_max_temp_per_year.png'
        plt.savefig(path_to_output_file)

    def _style_chart(self, ax: Axes, title: str):
        # Edit the chart
        plt.title(title)
        ax.legend(loc='upper right')

        # Edit the axes
        ax.set_xlabel('Day')
        ax.set_ylabel('Temperature (°C)')
        plt.xlim(self.df['day_of_year'].min() - 1, self.df['day_of_year'].max())
        ax.xaxis.set_major_locator(mdates.MonthLocator())
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%B'))
        ax.xaxis.set_minor_locator(mdates.DayLocator())
