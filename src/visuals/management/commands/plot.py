import logging

from django.core.management import BaseCommand

from locations.models import Location
from measurements.models import Measurement
from visuals.plotter import Plotter

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Plot the measurements for Amsterdam'

    def handle(self, *args, **options):
        amsterdam = Location.objects.get(name='Amsterdam')
        qs = Measurement.objects.filter(location=amsterdam)
        plotter = Plotter(qs)
        plotter.plot_per_year('avg', smooth_days=30)
        plotter.plot_per_year('min', smooth_days=30)
        plotter.plot_per_year('max', smooth_days=30)
        plotter.plot_min_max_averaged_over_year(smooth_days=None)
