from collections import defaultdict

from enum import Enum

from common.utils import group_by_item


class DataImportError(Exception):
    pass


class ErrorType(Enum):
    MISSING_TEMPERATURE = 'missing_temperature'


class ErrorTracker:
    def __init__(self):
        self._errors = defaultdict(list)

    def has_errors(self):
        return bool(self._errors)

    def add_error(self, error_type: ErrorType, location_name: str, day: str):
        self._errors[error_type.value].append((location_name, day))

    def raise_error_if_any(self):
        """
        Raises DataImportError if there are any errors.
        """

        if self.has_errors():
            raise DataImportError(self.error_msg)

    @property
    def error_msg(self) -> str:
        error_msg = ''

        no_temperature_msg = []
        location_date_pairs = self._errors[ErrorType.MISSING_TEMPERATURE.value]
        for location, pairs_per_location in group_by_item(location_date_pairs, 0):
            dates = sorted([pair[1] for pair in pairs_per_location])
            if len(dates) == 1:
                dates_str = dates[0]
            else:
                dates_str = f'{dates[0]} - {dates[-1]}'
            no_temperature_msg.append(f'There is no temperature data for {location} on {dates_str}')
        error_msg += '. '.join(no_temperature_msg)

        return error_msg
