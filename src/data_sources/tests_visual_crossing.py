import datetime
from decimal import Decimal
from django.test import TestCase
from requests import HTTPError, Response
from unittest.mock import patch

from common.test_utils import full_suite
from data_sources.visual_crossing import VisualCrossingService, VisualCrossingResponse
from locations.models import Location
from measurements.models import Measurement

expected_result: VisualCrossingResponse = {
    'latitude': 52.516,
    'longitude': 13.3769,
    'address': 'Berlin',
    'days': [
        {'datetime': '2019-01-01', 'datetimeEpoch': 1546297200, 'tempmax': 8.0, 'tempmin': 4.0, 'temp': 6.7,
         'feelslikemax': 5.2, 'feelslikemin': -1.9, 'feelslike': 2.2, 'dew': 3.6, 'humidity': 81.0, 'precip': 2.1,
         'precipprob': 100.0, 'precipcover': 66.67, 'preciptype': ['rain'], 'snow': 0.0, 'snowdepth': 0.0,
         'windgust': 63.0,
         'windspeed': 37.1, 'winddir': 274.0, 'pressure': None, 'cloudcover': 58.9, 'visibility': 9.1,
         'solarradiation': 13.6, 'solarenergy': 1.1, 'uvindex': 1.0, 'sunrise': '08:17:23', 'sunriseEpoch': 1546327043,
         'sunset': '16:02:38', 'sunsetEpoch': 1546354958, 'moonphase': 0.91, 'conditions': 'Rain, Partially cloudy',
         'description': 'Partly cloudy throughout the day with a chance of rain throughout the day.', 'icon': 'rain',
         'stations': ['10385099999', 'EDDB', 'E2835', 'EDDT'], 'source': 'obs'},
        {'datetime': '2019-01-02', 'datetimeEpoch': 1546383600, 'tempmax': 4.0, 'tempmin': -1.8, 'temp': 2.1,
         'feelslikemax': -1.0, 'feelslikemin': -6.0, 'feelslike': -2.8, 'dew': -4.6, 'humidity': 61.8, 'precip': 0.2,
         'precipprob': 100.0, 'precipcover': 8.33, 'preciptype': ['rain'], 'snow': 0.0, 'snowdepth': 0.0,
         'windgust': 63.0,
         'windspeed': 34.8, 'winddir': 322.4, 'pressure': None, 'cloudcover': 29.6, 'visibility': 10.0,
         'solarradiation': 22.2, 'solarenergy': 1.8, 'uvindex': 1.0, 'sunrise': '08:17:15', 'sunriseEpoch': 1546413435,
         'sunset': '16:03:44', 'sunsetEpoch': 1546441424, 'moonphase': 0.95, 'conditions': 'Rain, Partially cloudy',
         'description': 'Partly cloudy throughout the day with morning rain.', 'icon': 'rain',
         'stations': ['10385099999', 'EDDB', 'E2835', 'EDDT'], 'source': 'obs'}
    ]}


def http_error(status_code: int, message: str) -> HTTPError:
    e = HTTPError(message)
    e.response = Response()
    e.response.status_code = status_code
    return e


out_of_credits_error = http_error(429, 'Out of credits')
authentication_error = http_error(401, 'Authentication error')


def patch_fetch_weather_for_location_and_date_range(credits_left: int = 1000, raised_error: HTTPError = None):
    def mock_fetch_weather_for_location_and_date_range(self, _location, start, end):
        nr_days = (end - start).days + 1
        if nr_days > credits_left:
            raise out_of_credits_error
        return nr_days

    if raised_error:
        return patch.object(VisualCrossingService, '_fetch_weather_for_location_and_date_range',
                            side_effect=raised_error)
    else:
        return patch.object(VisualCrossingService, '_fetch_weather_for_location_and_date_range',
                            mock_fetch_weather_for_location_and_date_range)


class VisualCrossingServiceTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.service = VisualCrossingService()
        cls.amsterdam = Location.objects.create(name='Amsterdam', latitude=52.3699, longitude=4.90788, altitude=-2)
        cls.berlin = Location.objects.create(name='Berlin', latitude=52.516, longitude=13.3769, altitude=35)

    # fetch_weather

    def test_fetch_weather(self):
        return_values = [
            (self.amsterdam.name, 50, False),
            (self.berlin.name, 50, False),
            (self.amsterdam.name, 15, False),
            (None, 0, True)
        ]
        with patch.object(VisualCrossingService, '_fetch_weather_chunk', side_effect=return_values):
            nr_locations, nr_measurements = self.service.fetch_weather(Location.objects.all())

        self.assertEqual(2, nr_locations)
        self.assertEqual(115, nr_measurements)

    # _fetch_weather_chunk

    def test_fetch_weather_chunk_not_out_of_credits(self):
        with patch_fetch_weather_for_location_and_date_range(1000):
            location_in_chunk, nr_fetched_measurements_in_chunk, out_of_credits = \
                self.service._fetch_weather_chunk(Location.objects.all(), chunk_size=50)
        self.assertEqual(self.amsterdam.name, location_in_chunk)
        self.assertEqual(50, nr_fetched_measurements_in_chunk)
        self.assertFalse(out_of_credits)

    def test_fetch_weather_chunk_almost_out_of_credits(self):
        with patch_fetch_weather_for_location_and_date_range(15):
            location_in_chunk, nr_fetched_measurements_in_chunk, out_of_credits = \
                self.service._fetch_weather_chunk(Location.objects.all(), chunk_size=50)
        self.assertEqual(self.amsterdam.name, location_in_chunk)
        self.assertEqual(13, nr_fetched_measurements_in_chunk)
        self.assertFalse(out_of_credits)

    def test_fetch_weather_chunk_out_of_credits(self):
        with patch_fetch_weather_for_location_and_date_range(0):
            location_in_chunk, nr_fetched_measurements_in_chunk, out_of_credits = \
                self.service._fetch_weather_chunk(Location.objects.all(), chunk_size=50)
        self.assertEqual(None, location_in_chunk)
        self.assertEqual(0, nr_fetched_measurements_in_chunk)
        self.assertTrue(out_of_credits)

    def test_fetch_weather_chunk_different_http_error(self):
        with patch_fetch_weather_for_location_and_date_range(raised_error=authentication_error), \
                self.assertRaisesMessage(HTTPError, 'Authentication error'):
            self.service._fetch_weather_chunk(Location.objects.all(), chunk_size=50)

    # _get_weather_from_api

    @full_suite()
    def test_get_weather_from_api(self):
        # Note: running this test case costs 2 credits, from the 1,000 daily free credits on the free tier
        weather = self.service._get_weather_from_api('Berlin', '2019-01-01', '2019-01-02')
        weather = {k: v for k, v in weather.items() if k in expected_result}
        self.assertEqual(expected_result, weather)

    @full_suite()
    def test_get_weather_from_api_unknown_location(self):
        with self.assertRaisesMessage(HTTPError, '400 Client Error'):
            self.service._get_weather_from_api('XXX', start_date='2019-01-01', end_date='2019-01-02')

    def test_get_weather_from_api_missing_start_date(self):
        with self.assertRaisesMessage(ValueError, 'Start date must be in format YYYY-MM-DD'):
            self.service._get_weather_from_api('Berlin', start_date='', end_date='2019-01-01')

    def test_get_weather_from_api_wrong_start_date(self):
        with self.assertRaisesMessage(ValueError, 'Start date must be in format YYYY-MM-DD'):
            self.service._get_weather_from_api('Berlin', start_date='20190101', end_date='2019-01-02')

    def test_get_weather_from_api_missing_end_date(self):
        with self.assertRaisesMessage(ValueError, 'End date must be in format YYYY-MM-DD'):
            self.service._get_weather_from_api('Berlin', start_date='2019-01-01', end_date='')

    def test_get_weather_from_api_wrong_end_date(self):
        with self.assertRaisesMessage(ValueError, 'End date must be in format YYYY-MM-DD'):
            self.service._get_weather_from_api('Berlin', start_date='2019-01-01', end_date='20190102')

    # _get_most_recent_day_without_data

    def test_get_most_recent_day_without_data(self):
        Measurement.objects.bulk_create([
            Measurement(location=self.berlin, day=datetime.date(2022, 6, day)) for day in range(11, 21)
        ])
        with patch.object(VisualCrossingService, '_yesterday', return_value=datetime.date(2022, 6, 30)):
            self.assertEqual(datetime.date(2022, 6, 30), self.service._get_most_recent_day_without_data(self.berlin))
        with patch.object(VisualCrossingService, '_yesterday', return_value=datetime.date(2022, 6, 21)):
            self.assertEqual(datetime.date(2022, 6, 21), self.service._get_most_recent_day_without_data(self.berlin))
        with patch.object(VisualCrossingService, '_yesterday', return_value=datetime.date(2022, 6, 20)):
            self.assertEqual(datetime.date(2022, 6, 10), self.service._get_most_recent_day_without_data(self.berlin))
        with patch.object(VisualCrossingService, '_yesterday', return_value=datetime.date(2022, 6, 11)):
            self.assertEqual(datetime.date(2022, 6, 10), self.service._get_most_recent_day_without_data(self.berlin))
        with patch.object(VisualCrossingService, '_yesterday', return_value=datetime.date(2022, 6, 10)):
            self.assertEqual(datetime.date(2022, 6, 10), self.service._get_most_recent_day_without_data(self.berlin))

    # _get_location_and_most_recent_day_without_data

    def test_get_location_and_most_recent_day_without_data(self):
        Measurement.objects.bulk_create([Measurement(location=self.berlin, day=datetime.date(2022, 6, day))
                                         for day in range(11, 21)] +
                                        [Measurement(location=self.amsterdam, day=datetime.date(2022, 7, day))
                                         for day in range(11, 21)])
        with patch.object(VisualCrossingService, '_yesterday', return_value=datetime.date(2022, 7, 30)):
            # Both Berlin and Amsterdam don't have data. We take the first location alphabetically by name
            self.assertEqual((self.amsterdam, datetime.date(2022, 7, 30)),
                             self.service._get_location_and_most_recent_day_without_data(Location.objects.all()))
        with patch.object(VisualCrossingService, '_yesterday', return_value=datetime.date(2022, 7, 15)):
            # Amsterdam has data for 15 July, Berlin does not
            self.assertEqual((self.berlin, datetime.date(2022, 7, 15)),
                             self.service._get_location_and_most_recent_day_without_data(Location.objects.all()))

    # _get_days_without_data

    def test_get_days_without_data(self):
        Measurement.objects.bulk_create([Measurement(location=self.berlin, day=datetime.date(2022, 6, day))
                                         for day in range(11, 21)])
        with patch.object(VisualCrossingService, '_yesterday', return_value=datetime.date(2022, 6, 25)):
            self.assertEqual((datetime.date(2022, 6, 21), datetime.date(2022, 6, 25)),
                             self.service._get_days_without_data(self.berlin, chunk_size=10))
        with patch.object(VisualCrossingService, '_yesterday', return_value=datetime.date(2022, 6, 20)):
            self.assertEqual((datetime.date(2022, 6, 1), datetime.date(2022, 6, 10)),
                             self.service._get_days_without_data(self.berlin, chunk_size=10))
        with patch.object(VisualCrossingService, '_yesterday', return_value=datetime.date(2022, 6, 9)):
            self.assertEqual((datetime.date(2022, 5, 31), datetime.date(2022, 6, 9)),
                             self.service._get_days_without_data(self.berlin, chunk_size=10))

    # _get_date_range

    def test_get_date_range(self):
        days = [datetime.date(2022, 6, 15), datetime.date(2022, 6, 14),
                datetime.date(2022, 6, 13), datetime.date(2022, 6, 11)]
        self.assertEqual((days[2], days[0]), self.service._get_date_range(days, chunk_size=3))

        days = [datetime.date(2022, 6, 15)]
        self.assertEqual((days[0], days[0]), self.service._get_date_range(days, chunk_size=3))

    # _convert_to_measurements

    def test_convert_to_measurements(self):
        measurements = VisualCrossingService()._convert_to_measurements(self.berlin, expected_result)
        self.assertEqual(2, len(measurements))
        self.assertEqual(self.berlin, measurements[0].location)
        self.assertEqual(datetime.date(2019, 1, 1), measurements[0].day)
        self.assertEqual(Decimal('16.0'), measurements[0].precipitation_duration)

    def test_convert_to_measurements_with_missing_temperature(self):
        result_with_missing_temp = expected_result.copy()
        result_with_missing_temp['days'][1]['temp'] = None
        vc = VisualCrossingService()
        measurements = vc._convert_to_measurements(self.berlin, result_with_missing_temp)

        # Verify that the correct measurement is still converted
        self.assertEqual(1, len(measurements))

        # Verify that for the incorrect measurement, an error has been tracked
        vc._error_tracker.has_errors()
