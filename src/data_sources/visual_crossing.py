"""
Visual Crossing Weather Data Source

This module provides a class for retrieving weather data from the Visual Crossing Weather API
The API is documented at https://www.visualcrossing.com/resources/documentation/weather-api/
Data is documented at https://www.visualcrossing.com/resources/documentation/weather-data/weather-data-documentation/
We use the free tier of the API, which allows for 1,000 data points per day
1 data point is equivalent to 1 day of weather data for 1 location
"""

import math
import requests
from datetime import datetime, date, timedelta
from decimal import Decimal
from django.conf import settings
from django.db.models import QuerySet
from typing import TypedDict, List, Optional, Set, Collection, Tuple

from data_sources.error_tracker import ErrorTracker, ErrorType
from locations.models import Location
from measurements.models import Measurement


class VisualCrossingDay(TypedDict, total=False):
    datetime: str  # "2020-01-12"
    datetimeEpoch: int  # 1578783600
    tempmax: float  # 7.9
    tempmin: float  # 5.1
    temp: float  # 6.1
    feelslikemax: float  # 4.6
    feelslikemin: float  # 0.5
    feelslike: float  # 1.7
    dew: float  # 3.5
    humidity: float  # 83.7
    precip: float  # 4.519
    precipprob: float  # 100.0
    precipcover: float  # 54.17
    preciptype: List[str]  # ["rain"]
    snow: float  # 0.0
    snowdepth: float  # 0.0
    windgust: Optional[float]  # null
    windspeed: float  # 31.8
    winddir: float  # 220.9
    pressure: float  # 1016.8
    cloudcover: float  # 92.0
    visibility: float  # 18.6
    solarradiation: float  # 6.6
    solarenergy: float  # 0.6
    uvindex: float  # 0.0
    sunrise: str  # "08:31:33"
    sunriseEpoch: int  # 1578814293
    sunset: str  # "16:25:07"
    sunsetEpoch: int  # 1578842707
    moonphase: float  # 0.52
    conditions: str  # "Rain, Overcast"
    description: str  # "Cloudy skies throughout the day with rain."
    icon: str  # "rain"
    stations: str  # [ "05280",; "C7997",; "01981",; "00760",; "04857",; "01975",; "04039"]
    source: str  # "obs"


class VisualCrossingResponse(TypedDict):
    latitude: float  # 52.516
    longitude: float  # 13.3769
    address: str  # "Berlin"
    days: List[VisualCrossingDay]


class VisualCrossingService:
    api_key = settings.GET_SECRET('VISUAL_CROSSING_API_KEY')
    base_url = 'https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services'

    def __init__(self):
        self._error_tracker = ErrorTracker()

    def fetch_weather(self, locations: QuerySet) -> (int, int):
        """
        Fetch weather data from the Visual Crossing API for the given locations
        for the most recent days without data until we run out of credits, and store them

        Since we only can make 1,000 queries per day, we will check which location has the most recent day without data,
        and fetch weather data for that location for that day, and then repeat until we run out of credits.

        Note: This is not optimized for number of DB calls, nor for number of API calls.

        :param locations: The locations for which to fetch weather data
        :return: A tuple containing the number of locations for which weather data was fetched,
                 and the number of measurements that were fetched
        """

        out_of_credits = False
        locations_with_fetched_data: Set[str] = set()
        nr_measurements = 0
        while not out_of_credits:
            location_in_chunk, nr_measurements_in_chunk, out_of_credits = \
                self._fetch_weather_chunk(locations)
            if location_in_chunk:
                locations_with_fetched_data.add(location_in_chunk)
            nr_measurements += nr_measurements_in_chunk
        return len(locations_with_fetched_data), nr_measurements

    def _fetch_weather_chunk(self, locations: QuerySet, chunk_size: int = 50) -> Tuple[Optional[str], int, bool]:
        """
        Fetch weather data for a single location from the given input list of locations

        :param locations: The locations for which to fetch weather data
        :param chunk_size: Chunk size to use for fetching weather data
        :return: Tuple with the following information:
                    - Location name for which weather data was fetched, or None if no data was fetched at all
                    - The number of measurements that were fetched
                    - Whether we ran out of credits
        """

        location, _ = self._get_location_and_most_recent_day_without_data(locations)
        start, end = self._get_days_without_data(location, chunk_size)
        try:
            nr_measurements = self._fetch_weather_for_location_and_date_range(location, start, end)
        except requests.HTTPError as e:
            if e.response.status_code == 429:
                # Try again with a smaller chunk, until it works or we run out of credits
                if chunk_size > 1:
                    # Decrease the chunk size and try again
                    chunk_size = int(math.ceil(chunk_size / 2))  # 50, 25, 13, 7, 4, 2, 1
                    return self._fetch_weather_chunk(locations, chunk_size)
                else:
                    # We ran out of credits
                    return None, 0, True
            else:
                # Do not handle any axception from the API that is unrelated to running out of credits
                raise

        # If we get here, we did not run out of credits. However, if any error in the data occurs,
        # it is useless to continue, since we will not be able to use the stored data anyway.
        self._error_tracker.raise_error_if_any()

        return location.name, nr_measurements, False

    def _get_most_recent_day_without_data(self, location: Location) -> date:
        """
        Get the most recent day for which we have no weather data for the given location

        :param location: The location for which to check
        :return: The most recent day for which we have no weather data for the given location
        """

        days_with_data = set(Measurement.objects.filter(location=location).values_list('day', flat=True))
        day_to_check = self._yesterday()
        while True:
            if day_to_check not in days_with_data:
                return day_to_check
            day_to_check -= timedelta(days=1)

    def _get_location_and_most_recent_day_without_data(self, locations: QuerySet) -> (Location, date):
        """
        Get the location and most recent day for which we have no weather data

        :return: The location and most recent day for which we have no weather data
        """

        locations = locations.order_by('name')
        location_and_day = None
        for location in locations:
            day = self._get_most_recent_day_without_data(location)
            if location_and_day is None or day > location_and_day[1]:
                location_and_day = (location, day)
        return location_and_day

    def _get_days_without_data(self, location: Location, chunk_size: int) -> (date, date):
        """
        Get the start and end date for the most recent streak for which we have no weather data for the given location

        :param location: The location for which to check
        :param chunk_size: The number of consecutive days without data
        :return: The days for which we have no weather data for the given location, sorted from recent to old
        """

        days_with_data = set(Measurement.objects.filter(location=location).values_list('day', flat=True))
        days_without_data = set()
        years_back = 5
        while not days_without_data:
            days_to_check = {self._yesterday() - timedelta(days=i) for i in
                             range((years_back - 5) * 365, years_back * 365)}
            days_without_data = days_to_check - days_with_data
            years_back += 5
        return self._get_date_range(days_without_data, chunk_size)

    def _get_date_range(self, days: Collection[date], chunk_size: int) -> (date, date):
        """
        Get the start and end date for the most recent streak for the given days

        :param days: List of dates to give the date range for
        :param chunk_size: The number of consecutive days without data
        :return: The start and end date for the most recent streak for the given days
        """

        if not days:
            raise AssertionError('Cannot determine date range from empty list of days')

        days = sorted(days, reverse=True)
        days = days[:chunk_size]
        end_day = days[0]
        start_day = days[0]
        for day_index, day in enumerate(days[1:]):
            if day != end_day - timedelta(days=day_index + 1):
                break
            start_day = day
        return start_day, end_day

    def _fetch_weather_for_location_and_date_range(self, location: Location, start: date, end: date) -> int:
        """
        Fetch weather data from the Visual Crossing API for a single location and day, and store it

        :param location: The location for which to fetch weather data
        :param start: The start date for which to fetch weather data
        :param end: The end date for which to fetch weather data
        :return: The number of measurements that were fetched
        """

        measurements = self._get_measurements(location, start.strftime('%Y-%m-%d'), end.strftime('%Y-%m-%d'))
        Measurement.objects.bulk_create(measurements)
        return len(measurements)

    def _get_measurements(self, location: Location, start_date: str, end_date: str) -> List[Measurement]:
        """
        Get weather data from the Visual Crossing API for the given location and return them as Measurement objects.

        :param location: The location for which to retrieve weather data
        :param start_date: The start date for the weather data, in format YYYY-MM-DD
        :param end_date: The end date for the weather data, in format YYYY-MM-DD
        :return: A list of (in-memory) Measurement objects. Note: the callee is responsible for saving them
        """

        raw_measurements = self._get_weather_from_api(location.name, start_date, end_date)
        return self._convert_to_measurements(location, raw_measurements)

    def _get_weather_from_api(self, location: str, start_date: str, end_date: str) -> VisualCrossingResponse:
        """
        Get weather data from the Visual Crossing API.

        :param location: The location for which to retrieve weather data.
        :param start_date: The start date for the weather data, in format YYYY-MM-DD
        :param end_date: The end date for the weather data, in format YYYY-MM-DD
        """

        # Verify the dates are in the correct format
        if not self._is_valid_date(start_date):
            raise ValueError('Start date must be in format YYYY-MM-DD')
        if not self._is_valid_date(end_date):
            raise ValueError('End date must be in format YYYY-MM-DD')

        url = f'{self.base_url}/timeline/{location}/{start_date}/{end_date}'
        params = {'key': self.api_key, 'unitGroup': 'metric', 'contentType': 'json', 'include': 'days'}
        response = requests.get(url, params=params)
        response.raise_for_status()
        return response.json()

    @staticmethod
    def _yesterday() -> date:
        """
        Get the date of yesterday.

        :return: The date of yesterday
        """

        return date.today() - timedelta(days=1)

    @staticmethod
    def _is_valid_date(date_str: str) -> bool:
        """
        Verify that the date is in the correct format, YYYY-MM-DD.

        >>> VisualCrossingService()._is_valid_date('2021-01-01')
        True
        >>> VisualCrossingService()._is_valid_date('2021-01-1')
        False
        """

        try:
            datetime.strptime(date_str, '%Y-%m-%d')
            return True
        except (ValueError, TypeError):
            return False

    def _convert_to_measurements(self, location: Location,
                                 raw_measurements: VisualCrossingResponse) -> List[Measurement]:
        """
        Convert the raw measurements from the Visual Crossing API to Measurement objects
        """

        # Verify if the location is as expected
        location_from_service = Location(name=raw_measurements['address'],
                                         latitude=raw_measurements['latitude'],
                                         longitude=raw_measurements['longitude'])
        distance_threshold = 10
        if (distance := location_from_service - location) > distance_threshold:
            raise ValueError(f'Requested location ({location!r}) does not match '
                             f'location from service ({location_from_service!r}). '
                             f'Distance: {int(math.ceil(distance)):,} km > {distance_threshold:,} km')

        # Convert the raw measurements to Measurement objects
        result = []
        for raw_measurement in raw_measurements['days']:
            precip_duration = Decimal(raw_measurement['precipcover'] / 100 * 24).quantize(Decimal('0.1'))
            day = datetime.strptime(raw_measurement['datetime'], '%Y-%m-%d').date()
            if raw_measurement['temp'] is None:
                self._error_tracker.add_error(ErrorType.MISSING_TEMPERATURE, location.name, raw_measurement['datetime'])
            else:
                result.append(Measurement(location=location,
                                          day=day,
                                          temperature_avg=raw_measurement['temp'],
                                          temperature_min=raw_measurement['tempmin'],
                                          temperature_max=raw_measurement['tempmax'],
                                          feels_like_temperature_avg=raw_measurement['feelslike'],
                                          feels_like_temperature_min=raw_measurement['feelslikemin'],
                                          feels_like_temperature_max=raw_measurement['feelslikemax'],
                                          wind_speed=raw_measurement['windspeed'],
                                          precipitation=raw_measurement['precip'],
                                          precipitation_duration=precip_duration))
        return result
