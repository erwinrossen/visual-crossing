from django.test import TestCase

from data_sources.error_tracker import ErrorTracker, ErrorType
from locations.models import Location


class ErrorTrackerTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.amsterdam = Location.objects.create(name='Amsterdam', latitude=52.3699, longitude=4.90788, altitude=-2)
        cls.berlin = Location.objects.create(name='Berlin', latitude=52.516, longitude=13.3769, altitude=35)

    def test_has_errors(self):
        t = ErrorTracker()
        self.assertFalse(t.has_errors())

        t.add_error(ErrorType.MISSING_TEMPERATURE, 'Amsterdam', '2020-01-01')
        self.assertTrue(t.has_errors())

    def test_add_error(self):
        t = ErrorTracker()
        t.add_error(ErrorType.MISSING_TEMPERATURE, 'Amsterdam', '2020-01-01')
        t.add_error(ErrorType.MISSING_TEMPERATURE, 'Berlin', '2020-01-02')
        self.assertEqual(t._errors, {
            'missing_temperature': [
                ('Amsterdam', '2020-01-01'),
                ('Berlin', '2020-01-02'),
            ]
        })

    def test_error_msg(self):
        t = ErrorTracker()
        t.add_error(ErrorType.MISSING_TEMPERATURE, 'Amsterdam', '2020-01-01')
        t.add_error(ErrorType.MISSING_TEMPERATURE, 'Amsterdam', '2020-01-02')
        t.add_error(ErrorType.MISSING_TEMPERATURE, 'Amsterdam', '2020-01-03')
        t.add_error(ErrorType.MISSING_TEMPERATURE, 'Berlin', '2020-01-02')
        self.assertEqual(t.error_msg, 'There is no temperature data for Amsterdam on 2020-01-01 - 2020-01-03. '
                                      'There is no temperature data for Berlin on 2020-01-02')
