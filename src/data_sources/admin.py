import requests
from django.contrib import messages

from data_sources.visual_crossing import VisualCrossingService
from locations.admin import LocationAdmin


def fetch_measurements(_modeladmin, request, queryset):
    service = VisualCrossingService()
    try:
        nr_locations, nr_measurements = service.fetch_weather(queryset)
        if nr_measurements > 0:
            messages.success(request, f'Fetched {nr_measurements} measurements for {nr_locations} locations')
        else:
            messages.warning(request, f'No measurements were fetched, we are out of credits for today')
    except requests.HTTPError as e:
        messages.error(request, f'Failed to fetch weather: {e.response.status_code} - {e.response.content.decode()}')
    except Exception as e:
        messages.error(request, f'Failed to fetch weather: {e}')


# Add these actions dynamically on Location admin, to prevent a dependency from locations to data_sources
LocationAdmin.actions = (*LocationAdmin.actions, fetch_measurements)
